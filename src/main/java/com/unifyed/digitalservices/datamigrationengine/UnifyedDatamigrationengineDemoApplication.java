package com.unifyed.digitalservices.datamigrationengine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class UnifyedDatamigrationengineDemoApplication {
	  private static final Logger logger = LoggerFactory.getLogger(UnifyedDatamigrationengineDemoApplication.class);
	public static void main(String[] args) {
		ConfigurableApplicationContext context =
                SpringApplication.run(UnifyedDatamigrationengineDemoApplication.class, args);
        logger.warn("warning msg");
        logger.error("error msg");

	}
	
}
