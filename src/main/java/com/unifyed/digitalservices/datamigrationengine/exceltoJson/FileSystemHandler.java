package com.unifyed.digitalservices.datamigrationengine.exceltoJson;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileSystemHandler {
	
	public void handleFileSystemData(JSONObject jsonObj, ConfigurationExcelToJson config) throws JSONException {
		
		JSONArray filesArray = jsonObj.getJSONArray(Constants.FILESYSTEM);
		Map<String,String> fileMap = new HashMap<String,String>();
		for(int i =0;i<filesArray.length();i++) {
			
			JSONObject fileJsonObj= filesArray.getJSONObject(i);
			String fileName = fileJsonObj.getString(Constants.FILENAME);
			String filePath = fileJsonObj.getString(Constants.FILEPATH);
				
			fileMap.put(fileName, filePath);			
		}
		
		config.setFileMappings(fileMap);
	}

}
