package com.unifyed.digitalservices.datamigrationengine.exceltoJson;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ParsedSheet {

	private static final Logger LOG = LoggerFactory.getLogger(ParsedSheet.class);
	
    private Workbook workbook;
    private Sheet sheet;

    private ArrayList<ParsedCellType> types;
    private ArrayList<String> keys;

    public int typeRowIndex, nameRowIndex;
    public int width;

    private boolean parsed;

    public ParsedSheet(Workbook workbook, String sheetName) {
        this.workbook = workbook;
        sheet = workbook.getSheet(sheetName);

        if (sheet == null)
            throw new IllegalArgumentException("Unable to find the sheet name " + sheetName + " in the workbook.");

        typeRowIndex = 0;
        nameRowIndex = 1;

        width = 0;

        parsed = false;

        types = new ArrayList<>();
        keys = new ArrayList<>();
    }

    public ParsedSheet parseSheet(ConfigurationExcelToJson config) {
        if (parsed)
            return this;
        String camelCase = "";
        try {
            // Fetch the type row.
            String firstColumnValue = sheet.getRow(typeRowIndex).getCell(0).getStringCellValue();
            LOG.info("************** firstColumnValue  ******************" + firstColumnValue);
            if ( !ParsedCellType.isBasicType(firstColumnValue)) {
                // If the primary key doesn't have a type defined "Basic", then we'll think all the columns are basic type,
                // and the first row is name row.
                typeRowIndex = 0;
                nameRowIndex = 0;
                
                Row typeRow = sheet.getRow(typeRowIndex);
                for (Iterator<Cell> cellsIT = typeRow.cellIterator(); cellsIT.hasNext(); )
                {
                    Cell cell = cellsIT.next();
                   
                    String cellValue = cell.getStringCellValue();
                    if(config.getInputOutputMappings().containsKey(cellValue)) {
	                    String cellInputType  = config.getInputOutputMappings().get(cellValue).getInputDataType();	                   
	                    types.add(getCorrectDataType(cellInputType));
	                    String cellOutputName = config.getInputOutputMappings().get(cellValue).getOutputField();
	                    LOG.info("************** cellInputType  ******************" + cellInputType);
	                    LOG.info("************** cellOutputName  ******************" + cellOutputName);
                    }
                    else {
                    	 types.add(ParsedCellType.BASIC);
                    }
                }
            } else {
                // Else read the type of each column
                Row typeRow = sheet.getRow(typeRowIndex);
                for (Iterator<Cell> cellsIT = typeRow.cellIterator(); cellsIT.hasNext(); )
                {
                    Cell cell = cellsIT.next();
                    String cellType = cell.getStringCellValue();
                    types.add(ParsedCellType.fromString(cellType));
                }
            }

            // Fetch the name row.
            Row nameRow = sheet.getRow(nameRowIndex);
            for (Iterator<Cell> cellsIT = nameRow.cellIterator(); cellsIT.hasNext(); )
            {            	
                Cell cell = cellsIT.next();
                LOG.info("-----------------cell.getStringCellValue()  ------" + cell.getStringCellValue());
                
                String cellValue =  cell.getStringCellValue();
                if(config.getInputOutputMappings().containsKey(cellValue)) {
                    String cellOutputField  = config.getInputOutputMappings().get(cellValue).getOutputField(); 
                    
                    camelCase = camelCase(cellOutputField);
                	keys.add(camelCase);
                	
                    String cellOutputName = config.getInputOutputMappings().get(cellValue).getOutputField();
                    LOG.info("Before Value =" + cellValue);
                    LOG.info("After Value =" + cellOutputField);
                }
                else {
                	camelCase = camelCase(cell.getStringCellValue());
                	keys.add(camelCase);
                }
                
                
                width++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        parsed = true;

        return this;
    }
    
    private Boolean hasAllUpperCase(String inputString)
    {
    	int upperCase = 0;
    	for ( int i = 0; i < inputString.length() ; i++ ) {
            if (Character.isUpperCase(inputString.charAt(i))){ upperCase++; }
            
        }
    	
    	return (inputString.length() == upperCase);
    }
	/*
	 * Convert Given String to Camel Case i.e.
	 * Capitalize first letter of every word to upper case
	 */
    private String camelCase(String inputString)
	{
    	if(inputString.indexOf("_") !=  -1 || hasAllUpperCase(inputString))
    	{
    		inputString = inputString.replaceAll("_", " ").toLowerCase();
        	String result = "";
            if (inputString.length() == 0) {
                return result;
            }
            char firstChar = inputString.charAt(0);
            char firstCharToLowerCase = Character.toLowerCase(firstChar);
            LOG.info("firstCharToLowerCase =" + firstCharToLowerCase);
            result = result + firstCharToLowerCase;
            for (int i = 1; i < inputString.length(); i++) {
                char currentChar = inputString.charAt(i);
                char previousChar = inputString.charAt(i - 1);
                if (previousChar == ' ') {
                    char currentCharToUpperCase = Character.toUpperCase(currentChar);
                    result = result + currentCharToUpperCase;
                } else {
                    char currentCharToLowerCase = Character.toLowerCase(currentChar);
                    result = result + currentCharToLowerCase;
                }
            }
            LOG.info("camelCase =" + result);
            result = result.replaceAll(" ", "");
            return result;
    	}
    	else 
    	{
    		String result = "";
            if (inputString.length() == 0) {
                return result;
            }
            char firstChar = inputString.charAt(0);
            char firstCharToLowerCase = Character.toLowerCase(firstChar);
            LOG.info("firstCharToLowerCase =" + firstCharToLowerCase);
            result = result + firstCharToLowerCase;
            for (int i = 1; i < inputString.length(); i++) {                
                       result = result + inputString.charAt(i);
               
            }
            LOG.info("camelCase =" + result);
            return result;
    	}
    		 
            
    	
        
		
		
	}

    private ParsedCellType getCorrectDataType(String cellInputType) {
		switch (cellInputType) {
			case "String":
				return ParsedCellType.BASIC;
			case "DateTime":
				return ParsedCellType.DATETIME;
			default :
				return ParsedCellType.BASIC;
		}
	}

	public Workbook getWorkbook() {
        return workbook;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public Sheet getSheet(String sheetName) {
        Sheet sheet = workbook.getSheet(sheetName);
        if (sheet == null)
            throw new IllegalArgumentException("Unable to find the sheet name " + sheetName + " in the workbook.");
        return sheet;
    }

    public boolean isParsed() {
        return parsed;
    }

    public ParsedCellType getType(int index) {
        if (!isParsed())
            throw new NullPointerException("This sheet haven't been parsed, please call parseSheet() method first!");

        return types.get(index);
    }

    public String getKey(int index) {
        if (!isParsed())
            throw new NullPointerException("This sheet haven't been parsed, please call parseSheet() method first!");

        return keys.get(index);
    }

    public int indexOfKey(String key) {
        if (!isParsed())
            throw new NullPointerException("This sheet haven't been parsed, please call parseSheet() method first!");

        return keys.indexOf(key);
    }

}