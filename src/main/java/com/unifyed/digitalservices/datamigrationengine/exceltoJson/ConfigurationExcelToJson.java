package com.unifyed.digitalservices.datamigrationengine.exceltoJson;

import java.util.ArrayList;
import java.util.Map;

public class ConfigurationExcelToJson {
	
	private Map<String,String> fileMappings;
	private  Map<String, InputOutputMapping> inputOutputMappings;
	private String primaryKey;
	private String secondaryKey;
	private ArrayList<String> keysOfInputFields;
	private String outputFormat;
	private String outputFilePath;
	
	public String getSecondaryKey() {
		return secondaryKey;
	}
	public void setSecondaryKey(String secondaryKey) {
		this.secondaryKey = secondaryKey;
	}
	
	public String getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
	public Map<String, String> getFileMappings() {
		return fileMappings;
	}
	public void setFileMappings(Map<String, String> fileMappings) {
		this.fileMappings = fileMappings;
	}
	
	public Map<String, InputOutputMapping> getInputOutputMappings() {
		return inputOutputMappings;
	}
	public void setInputOutputMappings(Map<String, InputOutputMapping> inputOutputMappings) {
		this.inputOutputMappings = inputOutputMappings;
	}
	public ArrayList<String> getKeysOfInputFields() {
		return keysOfInputFields;
	}
	public void setKeysOfInputFields(ArrayList<String> keysOfInputFields) {
		this.keysOfInputFields = keysOfInputFields;
	}
	public String getOutputFormat() {
		return outputFormat;
	}
	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}
	public String getOutputFilePath() {
		return outputFilePath;
	}
	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}
	
	
}
