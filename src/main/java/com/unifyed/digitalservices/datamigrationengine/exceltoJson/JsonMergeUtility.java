package com.unifyed.digitalservices.datamigrationengine.exceltoJson;


import java.util.LinkedHashMap;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JsonMergeUtility {

	
	private static final Logger LOG = LoggerFactory.getLogger(JsonMergeUtility.class);
	
	private static JSONArray merge(JSONObject source, JSONObject target) throws JSONException {
		
		JSONArray jsonArray = new JSONArray();
		jsonArray.put(source);
		jsonArray.put(target);
		
	    return jsonArray;
	}

	public static JSONArray mergeFieldsOnPrimaryKey(JSONArray jsonArr,String matchCriteria) {
		
		JSONArray jsonArray = new JSONArray();
		 try {
         	for(int j =0;j<jsonArr.length();j++) {
         		boolean matchFound = false;
         		
         		JSONObject jsonObjSource = jsonArr.getJSONObject(j);
         		
         		 
         		 
         		for(int k=j+1;k<jsonArr.length();k++) {
         			JSONObject jsonObjTarget = jsonArr.getJSONObject(k);
         			
         			try {
						if(jsonObjTarget.get(matchCriteria).
								equals(jsonObjSource.get(matchCriteria))) {
							matchFound = true;
							JSONArray array = merge(jsonObjSource, jsonObjTarget);
							//jsonArr.put(j, jsonArr);
							jsonArray.put(array);
							jsonArr.remove(k);
 }
					} catch (Exception e) {
						 LOG.info("Given code not  found ->" + matchCriteria);
					}
         		}
         		if(!matchFound) {
         			jsonArray.put(jsonObjSource);
         		}		
         	}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		return jsonArray;
	}

	

	
	public static JSONArray addKeyToEveryObject(JSONArray jsonArr, String primaryKey, String secondaryKey) throws JSONException {
		
		JSONArray newJsonArr = new JSONArray();
		for(int i = 0; i<jsonArr.length(); i++) {
			String primaryValue="";
			String secondaryValue="";
			int primaryValInt=0;
			JSONObject jsonOb;
			if(jsonArr.get(i) instanceof JSONArray) {
				JSONArray jsonArray = jsonArr.getJSONArray(i);
				
				for(int j=0; j<jsonArray.length();j++) {
					JSONObject jsonObj = jsonArray.getJSONObject(j);
					jsonOb = jsonArray.getJSONObject(j);
					if(primaryKey.equalsIgnoreCase(Constants.PERSONPHONE_ID))
					{
						primaryValInt = jsonObj.getInt(primaryKey);
						if(jsonObj.has(secondaryKey))
							secondaryValue = jsonObj.getString(secondaryKey);
						
						jsonObj.remove(primaryKey);
					}
					else if(jsonObj.has(primaryKey))
					{
						primaryValue = jsonObj.getString(primaryKey);
						
						jsonObj.remove(primaryKey);
					}
					
					
				}
				if(primaryKey.equalsIgnoreCase(Constants.PERSONPHONE_ID))
				{					
					String key = secondaryValue + "_" + primaryValInt;
					if(secondaryValue.isEmpty())
						newJsonArr.put(new JSONObject().put("" + primaryValInt, jsonArray));
					else					
						newJsonArr.put(new JSONObject().put(key, jsonArray));
				}else {
					newJsonArr.put(new JSONObject().put(primaryValue, jsonArray));
				}
					
			}
			else {
				 LOG.info("=========== else=============== primaryKey =" + primaryKey);		
				 JSONObject jsonObj = jsonArr.getJSONObject(i);
				 
				 if(primaryKey.equalsIgnoreCase(Constants.PERSONPHONE_ID))
				 {
					if(jsonObj.has(secondaryKey))
						secondaryValue = jsonObj.getString(secondaryKey);

					int k = jsonObj.getInt(primaryKey);
					if(secondaryValue.isEmpty())
						newJsonArr.put(new JSONObject().put(""  + k, jsonObj));
					else
					{
						String key = secondaryValue + "_" + k;
						newJsonArr.put(new JSONObject().put(key, jsonObj));
					}
					jsonObj.remove(primaryKey);
				
					
				 }
				 else
				 {
					 if(jsonObj.has(primaryKey))
					 {
						primaryValue = jsonObj.getString(primaryKey);
						jsonObj.remove(primaryKey);
						newJsonArr.put(new JSONObject().put(primaryValue, jsonObj));
					 }
						
				 }
				
			}
		}
		return newJsonArr;
	}
	

	/*public static JSONArray mergeAllJsonArrays(LinkedList<JSONArray> listOfJsonArrays) throws JSONException {

		JSONArray newJsonArr = new JSONArray();
		boolean matchFound=false;
		for(int i = 0;i<listOfJsonArrays.size();i++) {
			JSONArray firstArray = listOfJsonArrays.get(i);
			
			for(int k= 0;k<firstArray.length();k++) {
				JSONObject jsonObjFirst = firstArray.getJSONObject(k);
				String[] primaryValueFirst = JSONObject.getNames(jsonObjFirst);
				JSONObject objectToAdd = new JSONObject();
				objectToAdd.put("Address", jsonObjFirst.get(primaryValueFirst[0]));
				
				//Append all matches into this
				for(int j = i+1 ; j<listOfJsonArrays.size() ;j++) {
					JSONArray secondArray = listOfJsonArrays.get(j);
					for(int l = 0;l<secondArray.length();l++) {
						JSONObject jsonObjSecond = secondArray.getJSONObject(l);
						String[] primaryValueSecond = JSONObject.getNames(jsonObjSecond);
						if(primaryValueFirst[0].equalsIgnoreCase(primaryValueSecond[0])) {
							objectToAdd.put("Communications", jsonObjSecond.get(primaryValueSecond[0]));
							secondArray.remove(l);
							break;
						}
					}
				}
				
				
					
					newJsonArr.put(new JSONObject()
										.put(primaryValueFirst[0],objectToAdd));
				

			}
		
		}
		return newJsonArr;
	}*/
	public static JSONArray changePeopleObjectFormat(JSONArray jsonArrays, ConfigurationExcelToJson config) throws JSONException {		
	
		JSONArray jsonFinalOutputArray = new JSONArray();
		int i = 0;
		for(int k= 0;k<jsonArrays.length();k++) {
			
			if(jsonArrays.getJSONObject(k).has(Constants.PEOPLE))
			{
				JSONObject personObject = jsonArrays.getJSONObject(k).getJSONObject(Constants.PEOPLE );
				
				for (String fileName : config.getFileMappings().keySet()) {
						
					if(!fileName.equalsIgnoreCase(Constants.PEOPLE))
					{
						Object obj = jsonArrays.getJSONObject(k).get(fileName);
						if (obj instanceof JSONArray) {
						    // It's an array
							personObject.put(fileName, jsonArrays.getJSONObject(k).getJSONArray(fileName));
						}
						else if (obj instanceof JSONObject) {
						    // It's an object
							personObject.put(fileName, jsonArrays.getJSONObject(k).getJSONObject(fileName));
						}
					}
					
					
			
												
				}
				LOG.info("personObject[" + k +  " ]"+ personObject);
				if(personObject.has("lastName"))
				{					
					
					jsonFinalOutputArray.put(i, personObject);
					LOG.info("-----IF------ i = [" +i +  " ]");
					 i++;
				}
					
				
				personObject = null;
			
				
			
			}
			else
			{
				LOG.info(" ===== >  i = [" +i +  " ]");
				LOG.info("JSONObject[\"People\"] not found");
			}
			
			
		}
		return jsonFinalOutputArray;
}
	
	public static LinkedHashMap<String, JSONArray> changeKeyIfDifferent(LinkedHashMap<String, JSONArray> mapOfJsonArrays) throws JSONException {
			
			JSONArray peopleArray = mapOfJsonArrays.get(Constants.PEOPLE);
			JSONArray phoneArray = mapOfJsonArrays.get(Constants.PHONES);
			LOG.info("peopleArray.length() =" + peopleArray.length());
			LOG.info("phoneArray.length() =" + phoneArray.length());
			LOG.info("phoneArray =" + phoneArray);
			LOG.info("peopleArray =" + peopleArray);
		
			for(int k= 0;k<peopleArray.length();k++) {
				JSONObject objectToAdd = new JSONObject();
				JSONObject peopleObj = peopleArray.getJSONObject(k);
				JSONArray primaryKey = peopleObj.names();
				JSONObject phoneObj = phoneArray.getJSONObject(k);
				//phoneArray.remove(k);
				objectToAdd.put(peopleObj.names().getString(0), phoneObj.get(phoneObj.names().getString(0)));
				phoneArray.put(k,objectToAdd);
			
			}
			
			//mapOfJsonArrays.remove(phoneArray);
			//phoneArray.put(new JSONObject()
					//.put(objectToAdd.names().getString(0),objectToAdd));
			//mapOfJsonArrays.put("Phone", phoneArray);
		
			return mapOfJsonArrays;
	}
	/*public static JSONArray finalModificationOnMergeAllJsonArrays(LinkedHashMap<String, JSONArray> mapOfJsonArrays) throws JSONException{
		
	}*/
	public static JSONArray mergeAllJsonArrays1(LinkedHashMap<String, JSONArray> mapOfJsonArrays) throws JSONException{
		
		LOG.info("--------------- merge All Json  ----------");		
		JSONArray newJsonArr = new JSONArray();
//		JSONArray peopleArray = mapOfJsonArrays.get(Constants.PEOPLE );		
		Set<String> keySet = mapOfJsonArrays.keySet();
		LOG.info("keySet =" + keySet);
		for(String keyFirst : keySet) {
			JSONArray firstArray = mapOfJsonArrays.get(keyFirst);
			
			LOG.info("keyFirst =" + keyFirst);
			LOG.info("firstArray.length() =" + firstArray.length());
			for(int k= 0;k<firstArray.length();k++) {
				JSONObject jsonObjFirst = firstArray.getJSONObject(k);
				JSONArray primaryValueFirst = jsonObjFirst.names();
				JSONObject objectToAdd = new JSONObject();
				objectToAdd.put(keyFirst, jsonObjFirst.get(primaryValueFirst.getString(0)));
				 
				//Append all matches into this
				for(String keySecond : keySet) {
					JSONArray secondArray = mapOfJsonArrays.get(keySecond);
					LOG.info("keySecond =" + keySecond);
					LOG.info("secondArray.length() =" + secondArray.length());
					for(int l = 0;l<secondArray.length();l++) {
						JSONObject jsonObjSecond = secondArray.getJSONObject(l);
						JSONArray primaryValueSecond = jsonObjSecond.names();
						String[]arrSecond = primaryValueSecond.getString(0).split("_");
						String[]arrFirst = primaryValueFirst.getString(0).split("_");
						LOG.info("primaryValueFirst=" + primaryValueFirst);
						LOG.info("primaryValueSecond=" + primaryValueSecond);
						if(primaryValueFirst.getString(0).equalsIgnoreCase(primaryValueSecond.getString(0)) && arrSecond.length< 2) {
							
												
							objectToAdd.put(keySecond, jsonObjSecond.get(primaryValueSecond.getString(0)));								
							LOG.info("keySecond=" + keySecond);
							LOG.info("jsonObjSecond.get(primaryValueSecond.getString(0)) =" + jsonObjSecond.get(primaryValueSecond.getString(0)));
							secondArray.remove(l);
							
					
						break;
							
						}
						else if(primaryValueFirst.getString(0).equalsIgnoreCase(arrSecond[0].toString()))
						{
							objectToAdd.put(keySecond, jsonObjSecond.get(primaryValueSecond.getString(0)));
							
							LOG.info("keySecond=" + keySecond);
							LOG.info("jsonObjSecond.get(primaryValueSecond.getString(0)) =" + jsonObjSecond.get(primaryValueSecond.getString(0)));
							secondArray.remove(l);
							break;
							
						}
						else if( arrFirst.length > 1 && primaryValueSecond.getString(0).equalsIgnoreCase(arrFirst[1].toString()))
						{
							objectToAdd.put(keySecond, jsonObjSecond.get(primaryValueSecond.getString(0)));
							
							LOG.info("keySecond=" + keySecond);
							LOG.info("jsonObjSecond.get(primaryValueSecond.getString(0)) =" + jsonObjSecond.get(primaryValueSecond.getString(0)));
							secondArray.remove(l);
							break;
						}
							
						
					}
				}
				//newJsonArr.put(new JSONObject()
							//.put(primaryValueFirst.getString(0),objectToAdd));
				newJsonArr.put(objectToAdd);
			}
		
		}
		return newJsonArr;	}
	
}
