package com.unifyed.digitalservices.datamigrationengine.exceltoJson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InputOutputMappingsHandler {

	public void handleIOMappingsData(JSONObject mappingsObject, ConfigurationExcelToJson config) throws JSONException {
		
		Map<String,InputOutputMapping> inputOutputMappings = new HashMap<String,InputOutputMapping>();
		ArrayList<String> keysOfInputFields = new ArrayList<String>();
		
		JSONArray ioArray = mappingsObject.getJSONArray(Constants.INPUTS_OUTPUTS);
		String primaryKey = mappingsObject.getString(Constants.PRIMARY_KEY);
		String secondaryKey = mappingsObject.getString(Constants.SECONDARY_KEY);
		for(int i =0;i<ioArray.length();i++) {
			
			JSONObject ioJsonObject= ioArray.getJSONObject(i);

			InputOutputMapping ioMapping = new InputOutputMapping();
			String inputField = ioJsonObject.getString(Constants.INPUT_FIELD);
			
			ioMapping.setInputField(inputField);
			ioMapping.setInputDataType(ioJsonObject.getString(Constants.INPUT_DATATYPE));
			ioMapping.setOutputField(ioJsonObject.getString(Constants.OUTPUT_FIELD));
			ioMapping.setOutputDataType(ioJsonObject.getString(Constants.OUTPUT_DATATYPE));
			inputOutputMappings.put(inputField, ioMapping);
			keysOfInputFields.add(inputField);
				
		}
		config.setInputOutputMappings(inputOutputMappings);
		config.setKeysOfInputFields(keysOfInputFields);
		config.setPrimaryKey(primaryKey);
		config.setSecondaryKey(secondaryKey);
	}

}
