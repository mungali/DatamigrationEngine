package com.unifyed.digitalservices.datamigrationengine.exceltoJson;

public class Constants {

	public static final String INPUT = "input";
	public static final String FILESYSTEM = "fileSystem";
	public static final String DATABASE = "database";
	public static final String Q = "q";
	public static final String FILENAME = "fileName";
	public static final String FILEPATH = "filePath";
	public static final String MAPPINGS = "mappings";
	public static final String INPUTS_OUTPUTS = "inputs-outputs";
	public static final String INPUT_FIELD = "input-field";
	public static final String INPUT_DATATYPE = "input-datatype";
	public static final String OUTPUT_FIELD = "output-field";
	public static final String OUTPUT_DATATYPE = "output-datatype";
	public static final String PRIMARY_KEY = "primaryKey";
	public static final String OUTPUT = "output";
	public static final String FORMAT = "format";
	public static final String JSON = "json";
	public static final String SECONDARY_KEY = "secondaryKey";
	public static final String PERSONPHONE_ID = "personPhoneId";
	public static final String PHONES = "Phones";
	public static final String PEOPLE = "People";
	//public static final String DEMOGRAPHICS = "Demographics";
	//public static final String ADDRESSES = "Addresses";
	
}
