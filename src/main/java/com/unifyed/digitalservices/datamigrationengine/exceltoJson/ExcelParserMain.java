package com.unifyed.digitalservices.datamigrationengine.exceltoJson; 



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/datamigrationengine/v1/exceltoJson")
@Api(tags = {"exceltoJson"})
public class ExcelParserMain {
	
	
	  private static final Logger LOG = LoggerFactory.getLogger(ExcelParserMain.class);
	  private static JSONArray jsonArray;
	  @RequestMapping(value = "",
	            method = RequestMethod.GET,
	            produces = {"application/json", "application/xml"})
	    @ResponseStatus(HttpStatus.OK)
	    @ApiOperation(value = "Get Json.", notes = "You can provide Excel")
	    public String exceltoJson() {
		  
		  long time = System.currentTimeMillis();
		  
		  LOG.info("************** Start  ******************");
		  
		  configuration();
		  
		  
		  LOG.info("  Total time taken [" + (time - System.currentTimeMillis()) +"]");
		  
		  LOG.info("************** End  ******************");
		  return jsonArray.toString();
	    }
	  
	  
	     
    /**
     * Read a workbook from the Excel file
     * @param targetName File name
     * @return The workbook
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static Workbook getWorkbook(String targetName) throws IOException, InvalidFormatException {
        File excelFile = new File(targetName + ".xlsx");
        FileInputStream inp = new FileInputStream( excelFile );
        Workbook workbook = WorkbookFactory.create( inp );
        return workbook;
    }

    /**
     * Construct a json object using the sheet list from the workbook
     * @param workbook
     * @param sheetList Sheet names in the workbook
     * @return Constructed json object
     *//*
    public static JSONObject constructJsonObject(Workbook workbook, String[] sheetList) {
        // Start constructing JSON.
        JSONObject json = new JSONObject();
        try {
	        // Create JSON
	        for (String sheetName : sheetList) {
	            JSONArray rows = ExcelParser.parseSheet(workbook, sheetName);
	            json.put(sheetName, rows);
	        }
        }
        catch (JSONException e1) {
            e1.printStackTrace();
        }
        return  json;
    }*/

    /**
     * Construct a json array using the sheet list from the workbook
     * @param workbook
     * @param sheetList
     * @param config 
     * @return
     */
    public static JSONArray constructJsonArray(Workbook workbook, String[] sheetList, ConfigurationExcelToJson config) {
        JSONArray json = new JSONArray();
        
        try {
	        // Create JSON
	        for (String sheetName : sheetList) {
	            JSONArray rows = ExcelParser.parseSheet(workbook, sheetName,config);
	            
	            LOG.info("JSONArray rows = " +  rows);
	            LOG.info("rows.length = " +  rows.length());
	            for (int i=0; i < rows.length(); i++) {
	            	LOG.info("=============== =[ " + i + "]");
	            	if(rows.getJSONObject(i).has("createDate"))
	            	{
	            		String createdate;
	            		String createtime;
	            		if(rows.getJSONObject(i).isNull("createDate") || rows.getJSONObject(i).isNull("createTime"))
	            			rows.getJSONObject(i).put("createDate", JSONObject.NULL);
	            		else
	            		{
	            			LocalDateTime time;
	            			createdate = rows.getJSONObject(i).getString("createDate");
	            			createtime = rows.getJSONObject(i).getString("createTime");
	            			String [] createtimearr = createtime.split(" ");
	            			String [] createdatearr = createdate.split(" ");
	            			String [] datesplitarr = createdatearr[0].split("/");
		            		String formatdate = createdatearr[0] + " " + createtimearr[1];
		            		if(formatdate.length()< 18)
		            			formatdate = datesplitarr[0] + "/" + datesplitarr[1] + "/20" + datesplitarr[2] + " " + createtimearr[1];
		            		
		            		time = LocalDateTime.parse(formatdate, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
		            		ZoneId zoneId = ZoneId.systemDefault();
		            		long epochSec = time.atZone(zoneId).toEpochSecond();
		            		rows.getJSONObject(i).put("createDate", epochSec);
	            		}
	            			            	
	            		
	            	}
	            	
	            	if(rows.getJSONObject(i).has("revisionDate"))
	            	{
	            		String date;
	            		String revisiondate;
	            		String revisiontime;
	            		if(rows.getJSONObject(i).isNull("revisionDate"))
	            			rows.getJSONObject(i).put("revisionDate", JSONObject.NULL);
	            		else
	            		{
		            		LocalDateTime time;
		            		revisiondate = rows.getJSONObject(i).getString("revisionDate");
		            		revisiontime = rows.getJSONObject(i).getString("revisionTime");
	            			String [] revisiontimearr = revisiontime.split(" ");
	            			String [] revisiondatearr = revisiondate.split(" ");
	            			String [] datesplitarr = revisiondatearr[0].split("/");
		            		String formatdate = revisiondatearr[0] + " " + revisiontimearr[1];
		            		if(formatdate.length()< 18)
		            			formatdate = datesplitarr[0] + "/" + datesplitarr[1] + "/20" + datesplitarr[2] + " " + revisiontimearr[1];
		            		
		            		time = LocalDateTime.parse(formatdate, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
		            		ZoneId zoneId = ZoneId.systemDefault();
		            		long epochSec = time.atZone(zoneId).toEpochSecond();
		            		rows.getJSONObject(i).put("revisionDate", epochSec);
	            		}
	            		
	            		
	            			            	
	            		
	            	}
	            	
	            	if(rows.getJSONObject(i).has("createTime"))
	            		rows.getJSONObject(i).remove("createTime");
	            	
	            	if(rows.getJSONObject(i).has("revisionTime"))
	            		rows.getJSONObject(i).remove("revisionTime");
	            	
	            	if(rows.getJSONObject(i).has("addressLine1") )
	            	{
	            		JSONArray lineArr = new JSONArray();
	            		
	            		if(rows.getJSONObject(i).isNull("addressLine1"))
	            		 {
	            			 rows.getJSONObject(i).remove("addressLine1");
	            			 rows.getJSONObject(i).remove("revisionOpid");
	            			 rows.getJSONObject(i).remove("createTerminal");
	            			 rows.getJSONObject(i).put("line", lineArr);
	            		 }
	            		 else
	            		 {
	            			 String lineValue = rows.getJSONObject(i).getString("addressLine1");
	            			 lineArr.put(lineValue);	
	 	            		rows.getJSONObject(i).put("line", lineArr);
	 	            		 rows.getJSONObject(i).remove("revisionOpid");
	 	            		rows.getJSONObject(i).remove("addressLine1");
	 	            		 rows.getJSONObject(i).remove("createTerminal");
	            		 }
	            		
	            	}
	            	
	            	if(rows.getJSONObject(i).has("addressLine2"))
	            	{
	            		 if(rows.getJSONObject(i).isNull("addressLine2"))
	            		 {
	            			 rows.getJSONObject(i).remove("addressLine2");
	            		 }
	            		 else
	            		 {
	            			 String addressLine2Value = rows.getJSONObject(i).getString("addressLine2");
	 	            		JSONArray lineArr = rows.getJSONObject(i).getJSONArray("line");
	 	            		lineArr.put(addressLine2Value);
	 	            		
	 	            		rows.getJSONObject(i).put("line", lineArr);
	 	            		rows.getJSONObject(i).remove("addressLine2");
	            		 }
	            		
	            	}
	            		
	            	
	            	if(rows.getJSONObject(i).has("addressLine3"))
	            	{
	            		 if(rows.getJSONObject(i).isNull("addressLine3"))
	            		 {
	            			 rows.getJSONObject(i).remove("addressLine3");
	            		 }
	            		 else
	            		 {
	            			 String addressLine3Value = rows.getJSONObject(i).getString("addressLine3");
	 	            		JSONArray lineArr = rows.getJSONObject(i).getJSONArray("line");
	 	            		lineArr.put(addressLine3Value);
	 	            		
	 	            		rows.getJSONObject(i).put("line", lineArr);
	 	            		rows.getJSONObject(i).remove("addressLine3");
	            		 }
	            		 
	            		
	            	}
	            	
	            	if(rows.getJSONObject(i).has("addressLine4"))
	            	{
	            		 if(rows.getJSONObject(i).isNull("addressLine4"))
	            		 {
	            			 rows.getJSONObject(i).remove("addressLine4");
	            		 }
	            		 else
	            		 {
	            			 String addressLine3Value = rows.getJSONObject(i).getString("addressLine4");
	 	            		 JSONArray lineArr = rows.getJSONObject(i).getJSONArray("line");
	 	            		 lineArr.put(addressLine3Value);
	 	            		 rows.getJSONObject(i).put("line", lineArr);
	 	            		 rows.getJSONObject(i).remove("addressLine4");
	            		 }
	            		 
	            		
	            	}
	            	
	               	if(rows.getJSONObject(i).has("countryId"))
	            	{
	               		if(rows.getJSONObject(i).has("personId"))
	            		 rows.getJSONObject(i).remove("personId");
	               		
	               		if(rows.getJSONObject(i).has("createTerminal"))
		            	 rows.getJSONObject(i).remove("createTerminal");
	            	}
	            	
	            	if(rows.getJSONObject(i).has("academicYear"))
	            	{
	            		//JSONArray academicArr = new JSONArray();
	            		JSONObject academicObj = new JSONObject();
	            		 if(rows.getJSONObject(i).isNull("academicYear"))
	            		 {
	            			 
	            			 rows.getJSONObject(i).remove("academicYear");
	            			 rows.getJSONObject(i).remove("peopleCode");
	            			 rows.getJSONObject(i).remove("createTerminal");
	            			 academicObj.put("academicYear", JSONObject.NULL);
	            			// academicArr.put(0, academicObj);
	            			 rows.getJSONObject(i).put("academics", academicObj);
	            		 }
	            		 else
	            		 {
	            			 int academicYearValue = rows.getJSONObject(i).getInt("academicYear");
	            			 academicObj.put("academicYear", academicYearValue);
	            			// academicArr.put(0, academicObj);
	            			// academicArr = rows.getJSONObject(i).getJSONArray("academics");	            			
	 	            		 rows.getJSONObject(i).put("academics", academicObj);
	 	            		 rows.getJSONObject(i).remove("academicYear");
	 	            		 rows.getJSONObject(i).remove("peopleCode");
	 	            		 rows.getJSONObject(i).remove("createTerminal");
	            		 }
	            		 
	            		
	            	}
	    			
	    	     	if(rows.getJSONObject(i).has("academicSession"))
	            	{
	    	     		//JSONArray academicArr = new JSONArray();
	            		JSONObject academicObj = new JSONObject();
	            		 if(rows.getJSONObject(i).isNull("academicSession"))
	            		 {
	            			 rows.getJSONObject(i).remove("academicSession");
	            			// academicArr = rows.getJSONObject(i).getJSONArray("academics");
	            			 academicObj = rows.getJSONObject(i).getJSONObject("academics");
	            			 academicObj.put("academicSession", JSONObject.NULL);
	            			// academicArr.put(0, academicObj);
	            			 rows.getJSONObject(i).put("academics", academicObj);
	            			
	            		 }
	            		 else
	            		 {
	            			 String academicSessionValue = rows.getJSONObject(i).getString("academicSession");
	            			// academicArr = rows.getJSONObject(i).getJSONArray("academics");
	            			 academicObj = rows.getJSONObject(i).getJSONObject("academics");
	            			 academicObj.put("academicSession", academicSessionValue);
	            			// academicArr.put(0, academicObj);
	 	            		 rows.getJSONObject(i).put("academics", academicObj);
	 	            		 rows.getJSONObject(i).remove("academicSession");
	            		 }
	            		 
	            		
	            	}
	    	     	
	    	     	if(rows.getJSONObject(i).has("academicTerm"))
	            	{
	    	     		JSONArray historiesArr = new JSONArray();
	            		JSONObject academicObj = new JSONObject();
	            		 if(rows.getJSONObject(i).isNull("academicTerm"))
	            		 {
	            			 rows.getJSONObject(i).remove("academicTerm");
	            			// academicArr = rows.getJSONObject(i).getJSONArray("academics");
	            			 academicObj = rows.getJSONObject(i).getJSONObject("academics");
	            			 academicObj.put("academicTerm", JSONObject.NULL);
	            			 academicObj.put("histories",historiesArr);
	            			// academicArr.put(0, academicObj);
	            			 rows.getJSONObject(i).put("academics", academicObj);
	            			 
	            		 }
	            		 else
	            		 {
	            			 String academicTermValue = rows.getJSONObject(i).getString("academicTerm");
	            			// academicArr = rows.getJSONObject(i).getJSONArray("academics");
	            			 academicObj = rows.getJSONObject(i).getJSONObject("academics");
	            			 academicObj.put("academicTerm", academicTermValue);
	            			 //academicArr.put(0, academicObj);
	 	            		 rows.getJSONObject(i).put("academics", academicObj);
	 	            		 rows.getJSONObject(i).remove("academicTerm");
	            		 }
	            		 
	            		
	            	}
	            	
	                json.put(rows.get(i));
	            }
	        }
        } 
        catch (JSONException e1) {
            e1.printStackTrace();
        }

        return json;
    }

    /**
     * Save a string as a json file
     * @param targetName
     * @param jsonText
     * @throws IOException
     */
    public static void saveStringToFile(String targetName, String jsonText) throws IOException {
        // Write into file
        Path path = Paths.get(targetName + ".json");
        BufferedWriter writer = Files.newBufferedWriter( path );
        LOG.info("************** Save a string as a json file  ******************");
        LOG.info("**************  Write into file Path ["+ path +"]");
        writer.write(jsonText);
        writer.close();
    }

    /**
     * Parse the excel file and save as json
     * @param targetName Excel file name without suffix
     * @param sheetList Target sheet list
     * @throws IOException 
     * @throws JSONException 
     */
    public static JSONArray parseExcelFile(String targetName,ConfigurationExcelToJson config) throws IOException, JSONException {

    	JSONArray json = null;
        try {
            Workbook workbook = getWorkbook(targetName);     
            
            String[] sheetList = new String[workbook.getNumberOfSheets()];
            
            for (int i=0; i<workbook.getNumberOfSheets(); i++) {
            	sheetList[i] = workbook.getSheetName(i) ;
            }
            json = constructJsonArray(workbook, sheetList,config);
            
            LOG.info("-----------------   constructJsonArray ->json = " + json);
            //Utility to search and merge with given field based on primaryKey
            LOG.info("-----------------BEFORE  mergeFieldsOnPrimaryKey ->Total Row = " + json.length());
            JSONObject jsonObjSource = json.getJSONObject(0);
            
            if(jsonObjSource.has(config.getSecondaryKey()))
            	json = JsonMergeUtility.mergeFieldsOnPrimaryKey(json,config.getSecondaryKey());
            else
            	json = JsonMergeUtility.mergeFieldsOnPrimaryKey(json,config.getPrimaryKey());                    
           
            LOG.info("-----------------   mergeFieldsOnPrimaryKey ->json = " + json);
            LOG.info("-----------------AFTER   mergeFieldsOnPrimaryKey ->Total Row = " + json.length());
            
            if(jsonObjSource.has(config.getSecondaryKey()) )
            {
            	String key = config.getSecondaryKey() +"_" + config.getPrimaryKey();
           	 	json = JsonMergeUtility.addKeyToEveryObject(json, config.getSecondaryKey(), config.getPrimaryKey());
            }
            else
            	 json = JsonMergeUtility.addKeyToEveryObject(json,config.getPrimaryKey(), config.getSecondaryKey());
            
           
            LOG.info("-----------------AFTER   addKeyToEveryObject ->Total Row = " + json.length());
            LOG.info("-----------------   addKeyToEveryObject ->json = " + json);
            
//            json = JsonMergeUtility.changeKeyIfDifferent(json) 
            
            return json;
           
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        return json;
    }

    /**
     * Validate two json file.  If it isn't it throws an {@link AssertionError}.
     * @param expectedFileName Expected JSON file
     * @param targetFileName File to compare
     * @param strict Enables strict checking
     * @throws JSONException
     */
    public static void validateJson(String expectedFileName, String targetFileName, boolean strict) {
        try {
            System.out.println("Checking " + targetFileName + " ... ");
            Path path = Paths.get(targetFileName);
            BufferedReader reader = Files.newBufferedReader(path);
            String jsonText = reader.readLine();

            path = Paths.get(expectedFileName);
            reader = Files.newBufferedReader(path);
            String expected = reader.readLine();

           // JSONAssert.assertEquals(expected, jsonText, false);

            System.out.println("Passed.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*catch (JSONException e1) {
            e1.printStackTrace();
        }*/
    }

    public String readFile(String filename) {
        String result = "";
        try {
        	ClassLoader classLoader = getClass().getClassLoader();
        	//File file = new File(classLoader.getResource.getFile());
        	InputStream stream = classLoader.getResourceAsStream(filename);
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            result = sb.toString();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    private void configuration() {
    	
    	 String jsonData = readFile("config.json");
    	 try {
			JSONObject jobj = new JSONObject(jsonData);
			
			/* ------------------------- Input Configuration--------------------------------------------- */
			LOG.info("----------------- Input Configuration------------------");
	    	JSONObject inputObject = jobj.getJSONObject(Constants.INPUT);
	    	ConfigurationExcelToJson config = new ConfigurationExcelToJson();
	    	JSONArray inputType = inputObject.names();
	    	String code;
	    	
	    	if(inputType != null ) {
	    		
	    		code = inputType.get(0).toString();
	    		
	    		switch (code) {

		    	case Constants.FILESYSTEM:
		    		new FileSystemHandler().handleFileSystemData(inputObject, config);
		    		LOG.info(Constants.FILESYSTEM);	
		    		System.out.println(Constants.FILESYSTEM);
		    		break;
		    	case Constants.DATABASE:
		    		System.out.println(Constants.DATABASE);
		    		break;
		    	case Constants.Q:
		    		LOG.info(Constants.Q);		    		
		    		break;
		    	default:
		    		LOG.info("Unkown error");
		    	

	    	 }
	    	
	    	}
	    	
	    

	    

	    	 
	    	
	    	
	    	/* ------------------------- Input/Output Mappings Configuration----------------------------- */
	    	LOG.info("-- Input/Output Mappings Configuration----------------");
	    	JSONObject mappingsObject = jobj.getJSONObject(Constants.MAPPINGS);
	    	JSONArray mappingsType = mappingsObject.names();
	    	if(mappingsType!= null) {
	    		new InputOutputMappingsHandler().handleIOMappingsData(mappingsObject,config);	
	    	}
	    	
	    	 /*------------------------- Output Configuration--------------------------------------------- */
	    	LOG.info("--------------- Output Configuration----------------------");
	    	JSONObject outputObject = jobj.getJSONObject(Constants.OUTPUT);
	    	 JSONArray outputType = mappingsObject.names();
	    	if(outputType!= null) {
	    		new OutputMappingsHandler().handleOutputData(outputObject,config);	
	    	}
	    	
	    	
	    	/*------------------------------- Run Parser based on Configuration Settings-----------------------------------------------------*/
	    	LOG.info("--------------- Run Parser based on Configuration Settings----------------------");
	    	if(Constants.JSON.equalsIgnoreCase(config.getOutputFormat()))
	    			runParser(config);
	    	

		} catch (JSONException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}
				
    }

	/*private static void runParser(ConfigurationExcelToJson config) throws IOException, JSONException {
		
		LinkedList<JSONArray> listOfJsonArrays = new LinkedList<JSONArray>();
		for (String fileName : config.getFileMappings().keySet()) {
			
			String exactFilePath = config.getFileMappings().get(fileName);
			JSONArray jsonArray = parseExcelFile(exactFilePath.substring(0, exactFilePath.length()-5),config);
			
			
			// At this point, every individual file is converted to json in expected format, 
			//now we need to merge data of two or more files based on primary key
			listOfJsonArrays.add(jsonArray);
			saveStringToFile(exactFilePath.substring(0, exactFilePath.length()-5), jsonArray.toString());
		}
		 
		JSONArray jsonArray = JsonMergeUtility.mergeAllJsonArrays(listOfJsonArrays);
		saveStringToFile("D:\\Excel_To_Json_Migration_Engine\\TestData\\Final", jsonArray.toString());
		
	}  */
    
    private static void runParser(ConfigurationExcelToJson config) throws IOException, JSONException {
		
		LinkedHashMap<String,JSONArray> mapOfJsonArrays = new LinkedHashMap<String,JSONArray>();
		for (String fileName : config.getFileMappings().keySet()) {
			
			String exactFilePath = config.getFileMappings().get(fileName);
			LOG.info("--------------- Run Parser --- exactFilePath -> "+ exactFilePath);
			JSONArray jsonArray = parseExcelFile(exactFilePath.substring(0, exactFilePath.length()-5),config);
				
			// At this point, every individual file is converted to json in expected format, 
			//now we need to merge data of two or more files based on primary key
			mapOfJsonArrays.put(fileName, jsonArray);
			saveStringToFile(exactFilePath.substring(0, exactFilePath.length()-5), jsonArray.toString());
		}
		
		LOG.info("--------------- mergeAllJsonArrays1 ----------");
		mapOfJsonArrays = JsonMergeUtility.changeKeyIfDifferent(mapOfJsonArrays);
		JSONArray jsonArrays = JsonMergeUtility.mergeAllJsonArrays1(mapOfJsonArrays);
		jsonArray = JsonMergeUtility.changePeopleObjectFormat(jsonArrays, config);
		//JSONArray jsonArray = JsonMergeUtility.finalModificationOnMergeAllJsonArrays(mapOfJsonArrays);
		
		saveStringToFile(config.getOutputFilePath(), jsonArray.toString());
		
	}  

}
