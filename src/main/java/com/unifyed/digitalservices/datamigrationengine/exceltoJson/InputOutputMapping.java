package com.unifyed.digitalservices.datamigrationengine.exceltoJson;



public class InputOutputMapping {
	
	private String inputField;
	private String inputDataType;
	private String outputField;
	private String outputDataType;
	public String getInputField() {
		return inputField;
	}
	public void setInputField(String inputField) {
		this.inputField = inputField;
	}
	public String getInputDataType() {
		return inputDataType;
	}
	public void setInputDataType(String inputDataType) {
		this.inputDataType = inputDataType;
	}
	public String getOutputField() {
		return outputField;
	}
	public void setOutputField(String outputField) {
		this.outputField = outputField;
	}
	public String getOutputDataType() {
		return outputDataType;
	}
	public void setOutputDataType(String outputDataType) {
		this.outputDataType = outputDataType;
	}
	
	
	

}
