package com.unifyed.digitalservices.datamigrationengine.exceltoJson;

import org.json.JSONException;
import org.json.JSONObject;

public class OutputMappingsHandler {

	public void handleOutputData(JSONObject outputObject, ConfigurationExcelToJson config) throws JSONException {
		
		config.setOutputFilePath(outputObject.getString(Constants.FILEPATH));
		config.setOutputFormat(outputObject.getString(Constants.FORMAT));
		
	}

}
